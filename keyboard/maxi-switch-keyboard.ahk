﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; Keyboard shortcuts for me Maxi-Switch Keyboard
; https://deskthority.net/wiki/Maxi-Switch_ME_101

Menu, Tray, Icon, maxi-m.ico

scriptActive := True

ToggleScript()
{
  global scriptActive
  if scriptActive 
	{
		scriptActive := False
    Menu, Tray, Icon, maxi-m.ico
		; Tippy("Enabled Maxi Keyboard Hotkeys")
	}
	else
	{
		scriptActive := True
    Menu, Tray, Icon, maxi-m-red.ico
		; Tippy("Disabled Maxi Keyboard Hotkeys")
	}
}

isScriptActive()
{
  global scriptActive
	if scriptActive
		return False
	return True
}

#InputLevel 0 
^!+\::ToggleScript()


#If isScriptActive()

  #InputLevel 3
  \::Send {Backspace}

  #InputLevel 2
  ^\::Send {\}

  #InputLevel 0 
  ^!+\::ToggleScript()

  RAlt:: LWin

#If
