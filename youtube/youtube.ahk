﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

Menu, Tray, Icon, youtube.ico
Menu, Tray, Standard
Menu, Tray, Add
Menu, Tray, Add, Youtube Download, Menu

; maybe make it a right click tray option

^!r::
    Reload
    Return

^!y::
    Goto Menu
    Return
    
Menu: 
    DownloadYoutubeDL()
    Gui, New
    
    Gui, Add, Text,, Youtube link:
    Gui, Add, Edit, vURL w250, 
    Gui, Add, CheckBox, vVideo, Video
    Gui, Add, CheckBox, vAudio, Audio
    Gui, Add, CheckBox, vThumbnail, Thumbnail
    Gui, Add, Button, Default, Download

    Gui, Show
    Return

ButtonDownload:
    Gui, Submit, NoHide
    Gui, Destroy
    Gui, New

    Gui, Add, Text, , Downloading... %URL%
    Gui, Add, Text, , Will ding when done!
    Gui, Show

    options := ""
    if (Video = 0)
    {
        options := options . " --skip-download"
    }

    if (Thumbnail = 1)
    {
        options := options . " --write-thumbnail"
    }

    if (Audio = 1)
    {
        options := options . " --extract-audio --audio-format mp3 --audio-quality 0"
    }

    RunWait, %comspec% /c "youtube-dl.exe %options% --output out\`%(title)s.`%(ext)s " %URL% ,, Hide
    Run, out

    Gui, Destroy
    SoundPlay, *-1

    Return

; https://www.reddit.com/r/AutoHotkey/comments/bq7y97/hotkeying_youtubedl/
DownloadYoutubeDL() {
    Try
        Run, youtube-dl, , Min
    Catch
    {
        Gui, New
        Gui, Add, Text, , Downloading youtube-dl.exe
        Gui, Show
        URLDownloadToFile, https://yt-dl.org/latest/youtube-dl.exe, youtube-dl.exe
        IfNotExist, youtube-dl.exe
            ExitApp, 1
        Gui, Destroy
    }
    #Persistent
    Return
}